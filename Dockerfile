FROM golang:1.10

WORKDIR /go/src/gitlab.com/SchoolOrchestration/Stream.git
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["Stream.git"]