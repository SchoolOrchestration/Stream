package router

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	. "gitlab.com/SchoolOrchestration/Stream.git/backends"
	. "gitlab.com/SchoolOrchestration/Stream.git/stream"
	"log"
	"time"
)

func SetupRouter () *gin.Engine{
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.GET(`/health`, func (c *gin.Context) {
		c.JSON(200, gin.H{
			`status`: `up`,
		})
	})
	r.POST(`/v2/stream/:actor/:object/:objectid/:verb/:target/`, streamHandlerV2)
	r.POST(`/stream/`, streamHandler)
	return r
}

func streamHandler(c *gin.Context) {
	var postCustomHandle map[string]interface{}
	err := json.NewDecoder(c.Request.Body).Decode(&postCustomHandle)
	if err != nil {
		log.Print(err)
	}
	go Streamer(&Stream{
		`1`,
		`Stream`,
		`posts`,
		`1`,
		`stream`,
		time.Now(),
		postCustomHandle,
		Context{},
		[]string{},
	})
	c.JSON(201, gin.H{
		`stream`: `successful`,
	})
}

func streamHandlerV2(c *gin.Context) {
	var postCustomHandle map[string]interface{}
	err := json.NewDecoder(c.Request.Body).Decode(&postCustomHandle)
	if err != nil {
		log.Print(err)
	}
	stream := &Stream{
		c.Param(`objectid`),
		c.Param(`actor`),
		c.Param(`verb`),
		c.Param(`object`),
		c.Param(`target`),
		time.Now(),
		postCustomHandle,
		Context{},
		[]string{},
	}
	Streamer(stream)
	c.JSON(201, gin.H{
		`stream`: `successful`,
	})
}