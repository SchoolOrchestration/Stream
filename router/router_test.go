package router

import (
	"log"
	"net/url"
	"strings"
	"testing"
	"net/http/httptest"
	"net/http"
)


func getPOSTPayload() string {
	params := url.Values{}
	params.Add("test", "string")
	params.Add("testTwo", "1")

	return params.Encode()
}


func TestHealth(t *testing.T) {
	t.Run(`Test Health Endpoint`, func (t *testing.T) {
		r := SetupRouter()
		resp := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", `/health`, nil)
		r.ServeHTTP(resp, req)
		if resp.Code != 200 {
			t.Fatal(`Health Status is not 200`)
		}
	})
}

func TestStream(t *testing.T) {
	t.Run(`Test Stream Endpoint`, func (t *testing.T) {
		r := SetupRouter()
		resp := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", `/stream/`, strings.NewReader(getPOSTPayload()))
		r.ServeHTTP(resp, req)
		log.Print(resp.Code)
		if resp.Code != 201 {
			t.Fatal(`Stream Handler is not 201`)
		}
	})
}

func TestStreamV2(t *testing.T) {
	t.Run(`Test Stream V2 Endpoint`, func (t *testing.T) {
		r := SetupRouter()
		resp := httptest.NewRecorder()
		req, _ := http.NewRequest("POST", `/v2/stream/post/order/1/update/stream/`, strings.NewReader(getPOSTPayload()))
		r.ServeHTTP(resp, req)
		log.Print(resp.Code)
		if resp.Code != 201 {
			t.Fatal(`Stream Handler V2 is not 201`)
		}
	})
}