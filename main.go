package main // import "gitlab.com/SchoolOrchestration/Stream.git"

import (
	. "gitlab.com/SchoolOrchestration/Stream.git/router"
)

func main() {
	r := SetupRouter()
	r.Run(`0.0.0.0:8000`)
}
