package stream

import (
	"time"
	"encoding/json"
)

type Stream struct {
	ID string `json:"id"`
	Actor string `json:"actor"`
	Verb string `json:"verb"`
	Object string `json:"object"`
	Target string `json:"target"`
	TimeStamp time.Time `json:"timestamp"`
	Payload interface{} `json:"payload"`
	Context `json:"context"` //Optional
	Tags []string `json:"tags"`
}

type Context struct {
	Source string `json:"source"`
	Medium string `json:"medium"`
	Description string `json:"description"`
}

func JsonStringToStream(jsonStr string) (*Stream, error){
	var s *Stream
	if err := json.Unmarshal([]byte(jsonStr), &s); err != nil {return &Stream{}, err}
	s.TimeStamp = time.Now()
	return s, nil
}
