package stream

import (
	"log"
	"testing"
	"time"
)

func TestStream(t *testing.T) {
	t.Run(`Test Unmarshalling Payloads`, func (t *testing.T) {
		jsonStr := `{
						"id": "1", 
						"actor": "example_service",
						"verb": "post",
						"object": "1",
						"target": "locations",
						"payload": {
							"foo": "bar"
						},
						"context": {
							"source": "Blog Page",
							"medium": "mobile:ios",
							"description": "This is a test"
						}
					}`
		stream, err := JsonStringToStream(jsonStr)
		log.Print(err)
		if err != nil {
			t.Fatal(`Failed to Unmarshall Json String`)
		}
		if stream.ID != "1" {
			t.Fatal(`Invalid data returned in stream`)
		}
	})
	t.Run(`Test Unmarshalling Nested Payload`, func (t *testing.T) {
		jsonStr := `{
						"id": "2", 
						"actor": "example_service", 
						"verb": "post", 
						"object": "1", 
						"target": "locations", 
						"payload": {
							"bar": "baz", 
							"moreInfo": {
								"foo": "bar"
							}
						}
					}`
		stream, err := JsonStringToStream(jsonStr)
		if err != nil {
			t.Fatal(`Failed to Unmarshall Json String`)
		}
		if stream.ID != "2" {
			t.Fatal(`Invalid data returned in stream`)
		}
	})
	t.Run(`Test Unmarshalling Invalid Payload`, func (t *testing.T) {
		jsonStr := `{
						"id": "3", 
						"actor": "example_service", 
						"verb": "post", 
						"object": "1", 
						"target": "locations", 
						"payload": {
							"bar": "baz", 
							"moreInfo": {
								"foo": "bar"
							}S
						}
					}`
		_, err := JsonStringToStream(jsonStr)
		if err == nil {
			t.Fatal(`Failed to Handle Invalid Payload`)
		}
	})
	t.Run(`Test Unmarshalling TimeStamp`, func (t *testing.T) {
		jsonStr := `{
						"id": "2", 
						"actor": "example_service", 
						"verb": "post", 
						"object": "1", 
						"target": "locations", 
						"payload": {
							"bar": "baz", 
							"moreInfo": {
								"foo": "bar"
							}
						}
					}`
		stream, err := JsonStringToStream(jsonStr)
		if err != nil {
			t.Fatal(`Failed to Unmarshall Json String`)
		}
		if (stream.TimeStamp == time.Time{}) {
			t.Fatal(`Timestamp Not Set`)
		}
	})
}
