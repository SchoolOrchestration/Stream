package backends


import (
	"net/http/httptest"
	"net/http"
	"testing"
	"os"
)



func TestLogastashStreamData(t *testing.T) {
	t.Run(`Successful call to Log Stash`, func(t *testing.T) {
		mockLogstash := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusCreated)
		}))
		os.Setenv(`LOGSTASH_URL`, mockLogstash.URL)
		defer mockLogstash.Close()
		logstash := LogStash{
			mockLogstash.URL,
			``,
		}
		success := logstash.StreamData(&TestStream)
		if !success {
			t.Fatal(`Failed Streaming to Logstash`)
		}
	})
}