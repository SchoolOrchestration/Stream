package backends


import (
	"net/http/httptest"
	"net/http"
	"testing"
	"os"
)



func TestKeenStreamData(t *testing.T) {
	t.Run(`Successful call to Keen`, func(t *testing.T) {
		mockKeen := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusCreated)
		}))
		os.Setenv(`KEEN_URL`, mockKeen.URL)
		defer mockKeen.Close()
		keen := Keen{
			`1234abcd`,
			`1234-4321`,
			`abc123-456xyz`,
		}
		success := keen.StreamData(&TestStream)
		if !success {
			t.Fatal(`Failed Streaming to Keen`)
		}
	})
	t.Run(`Failed call to Keen`, func(t *testing.T) {
		mockKeen := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusBadGateway)
		}))
		os.Setenv(`KEEN_URL`, mockKeen.URL)
		defer mockKeen.Close()
		keen := Keen{
			`1234abcd`,
			`1234-4321`,
			`abc123-456xyz`,
		}
		success := keen.StreamData(&TestStream)
		if success {
			t.Fatal(`Keen Failure not handled`)
		}
	})
}