package backends

import (
	. "gitlab.com/SchoolOrchestration/Stream.git/stream"
	"testing"
	"time"
	"os"
	"net/http/httptest"
	"net/http"
)

type MockStream struct {}

func (*MockStream) StreamData (stream *Stream) bool {
	if (stream != &Stream{}) {
		return true
	}
	return false
}

var TestContext = Context{`unit_tests`, `go`,`testing`}
var TestStream = Stream{`1`,`test`, `test`,`1`,`testing`,time.Now(),
	``, TestContext, []string{`test`}}

func TestUploadStream(t *testing.T) {
	t.Run(`Test Stream Interface`, func (t *testing.T) {
		mockStream := MockStream{}
		response := UploadStream(&TestStream, &mockStream)
		if !response {
			t.Fatal(`Stream Interface Error`)
		}
	})
}

func TestStreamer(t *testing.T) {
	var calls []int
	expectedCallCount := 1
	mockKeen := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		calls = append(calls,1)
	}))
	os.Setenv(`KEEN_URL`, mockKeen.URL)
	defer mockKeen.Close()
	t.Run(`Test Streamer Runs All Configured Backends`, func (t *testing.T) {
		os.Setenv(`KEEN_COLLECTION`, `1234abcd`)
		os.Setenv(`KEEN_KEY`, `1234-4321`)
		os.Setenv(`KEEN_PROJECT_ID`, `abc123-456xyz`)
		Streamer(&TestStream)
		if len(calls) != expectedCallCount {
			t.Log(`Not all stream backends were called`)
		}
	})
}