package backends

import (
	. "gitlab.com/SchoolOrchestration/Stream.git/stream"
	"encoding/json"
	"net/http"
	"bytes"
	"os"
	)



type Keen struct {
	Collection string
	Key string
	Project string
}

func (k *Keen) StreamData(stream *Stream) bool {
	if k.Collection != `` && k.Key != `` && k.Project != `` {
		jsonBody, marshalErr := json.Marshal(stream)
		if marshalErr != nil {return false}
		keenUrl := os.Getenv(`KEEN_URL`)
		url := keenUrl + `/3.0/projects/` + k.Project + `/events/` + k.Collection + `?api_key=` + k.Key
		resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonBody))
		if err != nil || resp.StatusCode != 201 {
			return false
		}
		return true
	}
	return true
}

