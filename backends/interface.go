package backends

import (
	. "gitlab.com/SchoolOrchestration/Stream.git/stream"
	"os"
)


type StreamI interface {
	StreamData (*Stream) bool
}

func UploadStream (stream *Stream, streamBackend StreamI) bool{
	success := streamBackend.StreamData(stream)
	if !success {return false}
	return true
}


func Streamer (newStream *Stream) {
	// A list of Stream Handlers
	// Usage: Create a new backend that implements the StreamI Interface
	// Add the Struct below with the required settings
	StreamConfigs := []StreamI{
		&Keen{
			os.Getenv(`KEEN_COLLECTION`),
			os.Getenv(`KEEN_KEY`),
			os.Getenv(`KEEN_PROJECT_ID`),
		},
		&LogStash{
			os.Getenv(`LOGSTASH_URL`),
			os.Getenv(`LOGSTASH_PATH`),
		},
	}
	for  i := 0 ; i < len(StreamConfigs); i++ {
		UploadStream(newStream, StreamConfigs[i])
	}

}