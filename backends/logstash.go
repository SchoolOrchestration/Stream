package backends

import (
	"bytes"
	"encoding/json"
	. "gitlab.com/SchoolOrchestration/Stream.git/stream"
	"log"
	"net/http"
)



type LogStash struct {
	Url string
	Path string
}

func (l *LogStash) StreamData(stream *Stream) bool {
	if l.Url != `` {
		jsonBody, marshalErr := json.Marshal(stream)
		if marshalErr != nil {return false}
		req, err := http.NewRequest(http.MethodPut, l.Url, bytes.NewBuffer(jsonBody))
		req.Header.Add("Content-Type", "application/json")
		resp, err := http.DefaultClient.Do(req)
		if stream.ID != `1` {
			log.Print(`##########################################`)
			log.Print(resp.StatusCode)
			log.Print(stream)
			log.Print(`##########################################`)
		}

		if err != nil || resp.StatusCode != 201 {
			return false
		}
		return true
	}
	return true
}

